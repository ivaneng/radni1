
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LimuniService{

    constructor(private http:Http){}

    getLimunObservable(){
        return this.http.get('http://localhost:3000/limuni').map((response)=>{return response.json();});
    }

}