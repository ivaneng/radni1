
import { Injectable } from '@angular/core';
import { Limun } from "../interfaces/limun";
import { LimuniService } from './limuni.services';
@Injectable()
export class LimuniModel{

limuni  = [];
constructor(private service:LimuniService){

    this.service.getLimunObservable().subscribe((limuni)=>{
        this.limuni = limuni;
    })
}
}